package com.vgershman.truckviewtest.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.soundcloud.android.crop.Crop;
import com.vgershman.truckviewtest.R;
import com.vgershman.truckviewtest.data.RouteInfoDto;
import com.vgershman.truckviewtest.data.RouteManager;
import com.vgershman.truckviewtest.util.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, RoutingListener, GoogleMap.OnMarkerClickListener {

    private static final int REQ_CAMERA = 122;
    private static final int REQ_LOCATION = 123;
    private GoogleMap mMap;
    private Uri mImageUri;
    private Marker mImageMarker;
    private int mImageSize;
    private HashMap<Marker, String> mImagesMap = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        findViewById(R.id.btn_fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeShot();
            }
        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CAMERA && resultCode == Activity.RESULT_OK) {
            doCrop();
        }
        if (requestCode == Crop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            bindImage();
        }
        if (requestCode == REQ_LOCATION) {
            checkPermissionAndShowMyLocation();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        checkPermissionAndShowMyLocation();
        RouteManager.getRouteInfo(new Callback<RouteInfoDto>() {
            @Override
            public void success(RouteInfoDto routeInfoDto, Response response) {
                bindRoute(routeInfoDto);
            }

            @Override
            public void failure(RetrofitError error) {
                Snackbar.make(null, error.getMessage() + "", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (mImagesMap.containsKey(marker)) {
            Intent intent = new Intent(this, ImageViewerActivity.class);
            intent.putExtra(ImageViewerActivity.EXTRA_URL, mImagesMap.get(marker));
            startActivity(intent);
        }
        return false;
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        Snackbar.make(null, e.getMessage() + "", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onRoutingStart() {
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int i) {
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(Color.BLUE);
        polyOptions.width(10 + i * 3);
        polyOptions.addAll(route.get(0).getPoints());
        mMap.addPolyline(polyOptions);
    }

    @Override
    public void onRoutingCancelled() {
    }

    private void checkPermissionAndShowMyLocation() {
        if (Utils.isLocationEnbaled(this)) {
            mMap.setMyLocationEnabled(true);
        } else {
            openLocationSettings();
        }
    }

    private void openLocationSettings() {
        Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(viewIntent, REQ_LOCATION);
    }

    private void bindRoute(RouteInfoDto routeInfoDto) {
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.12); // offset from edges of the map 12% of screen
        mImageSize = (int) (width * 0.15);
        LatLng from = routeInfoDto.getFrom().getPlace().getLatLng();
        LatLng to = routeInfoDto.getTo().getPlace().getLatLng();
        mMap.addMarker(new MarkerOptions().position(from).title(routeInfoDto.getFrom().getStreet()).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_hospital_marker_black_48dp)));
        mMap.addMarker(new MarkerOptions().position(to).title(routeInfoDto.getTo().getStreet()).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_black_48dp)));
        LatLngBounds bounds = LatLngBounds.builder().include(from).include(to).build();
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding));
        RouteManager.buildRoute(from, to, this);
    }

    private void bindImage() {
        if (mMap.getMyLocation() != null) {
            LatLng me = new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude());
            mImageMarker = mMap.addMarker(new MarkerOptions().position(me).icon(BitmapDescriptorFactory.fromPath(mImageUri.getPath())));
            mImagesMap.put(mImageMarker, mImageUri.toString());
        }
    }

    private void takeShot() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mImageUri = Uri.fromFile(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "tmp_avatar__.jpg"));
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        takePictureIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 5 * 1024 * 1024);
        startActivityForResult(takePictureIntent, REQ_CAMERA);
    }

    private void doCrop() {
        Uri input;
        Uri output;
        input = mImageUri;
        mImageUri = Uri.fromFile(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "tmp_avatar_" +System.currentTimeMillis() + ".jpg"));
        output = mImageUri;
        final Uri input_ = input;
        Crop.of(input_, output).asSquare().withMaxSize(mImageSize, mImageSize).start(this);
    }
}
