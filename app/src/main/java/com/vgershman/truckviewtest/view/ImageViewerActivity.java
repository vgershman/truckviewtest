package com.vgershman.truckviewtest.view;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.vgershman.truckviewtest.R;

import uk.co.senab.photoview.PhotoViewAttacher;


/**
 * Created by user on 20.05.15.
 */
public class ImageViewerActivity extends Activity {

    public static final String EXTRA_URL = "extra_url";
    private boolean mToolbarVisible = true;
    private Uri mImageUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);
        mImageUrl = Uri.parse(getIntent().getStringExtra(EXTRA_URL));
        bindData();
        initButtons();
    }


    private void bindData() {
        if (mImageUrl == null) {
            onBackPressed();
        }
        Picasso.with(this).load(mImageUrl).into((ImageView) findViewById(R.id.iv_content), new Callback() {
            @Override
            public void onSuccess() {
                findViewById(R.id.progress).setVisibility(View.GONE);
                new PhotoViewAttacher((ImageView) findViewById(R.id.iv_content));
            }

            @Override
            public void onError() {
                findViewById(R.id.progress).setVisibility(View.GONE);
            }
        });
    }

    private void initButtons() {
        findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        findViewById(R.id.iv_content).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mToolbarVisible = !mToolbarVisible;
                findViewById(R.id.toolbar).setVisibility(mToolbarVisible ? View.VISIBLE : View.GONE);
            }
        });
    }
}
