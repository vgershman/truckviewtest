package com.vgershman.truckviewtest.data;


import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by user on 30.03.15.
 */
public interface RouteRestInterface {


    @GET("/bins/4jet0")
    void getRouteInfo(Callback<RouteInfoDto> callback);
}
