package com.vgershman.truckviewtest.data;

/**
 * Created by user on 30.03.16.
 */
public class RouteInfoDto {

    private LocationDto from_place;
    private LocationDto to_place;

    public LocationDto getFrom() {
        return from_place;
    }

    public LocationDto getTo() {
        return to_place;
    }
}
