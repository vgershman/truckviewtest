package com.vgershman.truckviewtest.data;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by user on 30.03.16.
 */
public class PlaceDto {

    private List<Double> coordinates;

    public LatLng getLatLng(){
        return new LatLng(coordinates.get(1), coordinates.get(0));
    }
}
