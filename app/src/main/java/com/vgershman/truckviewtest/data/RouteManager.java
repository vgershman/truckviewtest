package com.vgershman.truckviewtest.data;

import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.model.LatLng;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.android.AndroidLog;

/**
 * Created by user on 30.03.16.
 */
public class RouteManager {

    static final String API_URL = "https://api.myjson.com";

    public static void getRouteInfo(Callback<RouteInfoDto> routeInfoDtoCallback) {
        getRestService().getRouteInfo(routeInfoDtoCallback);
    }

    private static RouteRestInterface getRestService() {
        return new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(new AndroidLog("REST")).build()
                .create(RouteRestInterface.class);
    }

    public static void buildRoute(LatLng start, LatLng end, RoutingListener routingListener) {
        new Routing.Builder()
                .travelMode(Routing.TravelMode.DRIVING)
                .withListener(routingListener)
                .waypoints(start, end)
                .build().execute();
    }
}
