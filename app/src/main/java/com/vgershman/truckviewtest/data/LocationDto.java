package com.vgershman.truckviewtest.data;

/**
 * Created by user on 30.03.16.
 */
public class LocationDto {

    private int id;
    private String street;
    private PlaceDto place;

    public int getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public PlaceDto getPlace() {
        return place;
    }
}
