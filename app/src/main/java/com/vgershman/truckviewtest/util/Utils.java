package com.vgershman.truckviewtest.util;

import android.content.Context;
import android.location.LocationManager;

/**
 * Created by user on 31.03.16.
 */
public class Utils {

    public static boolean isLocationEnbaled(Context context) {
        LocationManager locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }
}
